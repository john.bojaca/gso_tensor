import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Schaffer"""
"""Mínimo Global => f(x) = 0, en x = (0,0)"""
class Schaffer(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -3.0, 3.0))
        self.setParameters(GSOParameters(find_min=True))
        self.setNumGlowworms(200)
        self.PlotSurface()

    def __call__(self, coordinates):
        return Schaffer.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        x = coordinates[:,0]
        y = coordinates[:,1]

        return 0.5 + ((torch.pow(torch.sin(torch.pow(x, 2) - torch.pow(y, 2)), 2) - 0.5) / (torch.pow(1 + 0.001*(torch.pow(x,2) + torch.pow(y, 2)), 2)))
    
    def PlotCalculate(self, x, y):
        return 0.5 + ((np.power(np.sin(np.power(x, 2) - np.power(y, 2)), 2) - 0.5) / (np.power(1 + 0.001*(np.power(x,2) + np.power(y, 2)), 2)))