import torch

class DSDeepSlice():
    def __init__(self, df):
        #xp = df.iloc[:,0:8].values
        #yp = df.iloc[:,8].values
        xp = df.iloc[:,0:22].values
        yp = df.iloc[:,22].values
        
        self.x = torch.tensor(xp, dtype = torch.float, device="cuda")
        self.y = torch.tensor(yp, dtype = torch.float, device="cuda")

    def __len__(self):
        return len(self.y)
    
    def __getitem__(self, idx):
        return self.x[idx], self.y[idx]
    
