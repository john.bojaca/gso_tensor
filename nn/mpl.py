from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space
from nn.neural_network import NeuralNetwork
import torch

class MPL(ObjectiveFunction, NeuralNetwork):
    def __init__(self):
        self.LoadData()
        self.CreateModel()

        self.parameters = GSOParameters(search_min=True)
        self.space = Space(self.GetVariables(), -1, 1.)

    """Carga la información para entrenamiento y test para Red Neuronal"""
    def LoadData(self):
        #self.data = torch.tensor([[0., 0., 0.], [0., 1., 0.], [1., 0., 0.], [1., 1., 1.]], device="cuda") ## Compuerta AND
        #self.data = torch.tensor([[0., 0., 0.], [0., 1., 1.], [1., 0., 1.], [1., 1., 1.]], device="cuda") ## Compuerta OR
        self.data = torch.tensor([[0., 0., 0.], [0., 1., 1.], [1., 0., 1.], [1., 1., 0.]], device="cuda") ## Compuerta XOR
        print(f"Datos Cargados ... Número de registros {self.data.size()}")

        self.LoadDataTraining()
        self.LoadDataTest()

    """Carga la información para el entrenamiento de la Red neuronal"""
    def LoadDataTraining(self):
        self.dataTrain = self.data
        print(f"Datos de Entrenamiento cargados ... Número de registros {self.dataTrain.size()}")

    """Carga la información para las pruebas de la red neuronal"""
    def LoadDataTest(self):
        self.dataTest = self.data
        print(f"Datos de Prueba cargados ... Número de registros {self.dataTest.size()}")

    """Retorna los datos de entrada para el entrenamiento"""
    def GetInputTraining(self):
        return self.dataTrain[:,:2]

    """Retorna los datos de salida para el entrenamiento"""
    def GetOutputTraining(self):
        return self.dataTrain[:,2]

    """Retorna los datos de entrada para las pruebas"""
    def GetInputTest(self):
        return self.dataTrain[:,:2]

    """Retorna los datos de salida para las pruebas"""
    def GetOutputTest(self):
        return self.dataTrain[:,2]

    """Crea el modelo de la red neuronal"""
    def CreateModel(self):
        print("Esta Red Neuronal crea el modelo en el entrenamiento")
    
    """Retornal el número de variables de la Red Neuronal"""
    def GetVariables(self):
        return 9

    """Llamado del Algoritmo GSO con las coordenadas de las Luciernagas"""
    def __call__(self, coordinates):
        return self.TrainNetwork(coordinates)

    """Entrena la Red Neuronal con el Algotirmo GSO"""
    def TrainNetwork(self, coordinates):
        w1 = coordinates[:,0]
        w2 = coordinates[:,1]
        w3 = coordinates[:,2]
        w4 = coordinates[:,3]
        w5 = coordinates[:,4]
        w6 = coordinates[:,5]
        bias1 = coordinates[:,6]
        bias2 = coordinates[:,7]
        bias3 = coordinates[:,8]
        
        w1 = w1.reshape(w1.size()[0], 1)
        w2 = w2.reshape(w2.size()[0], 1)
        w3 = w3.reshape(w3.size()[0], 1)
        w4 = w4.reshape(w4.size()[0], 1)
        w5 = w5.reshape(w5.size()[0], 1)
        w6 = w6.reshape(w6.size()[0], 1)
        bias1 = bias1.reshape(bias1.size()[0], 1)
        bias2 = bias2.reshape(bias2.size()[0], 1)
        bias3 = bias3.reshape(bias3.size()[0], 1)

        x1 = self.GetInputTraining()[:,0]
        x2 = self.GetInputTraining()[:,1]
        od = self.GetOutputTraining()

        n = od.size()[0]

        x1 = x1.reshape(1, x1.size()[0])
        x2 = x2.reshape(1, x2.size()[0])
        od = od.reshape(1, od.size()[0])

        z1 = w1@x1 + w3@x2 + bias1
        z2 = w2@x1 + w4@x2 + bias2

        #Capa RELU
        z1[z1 < 0] = 0
        z2[z2 < 0] = 0

        z = w5.repeat(1,4)*z1 + w6.repeat(1,4)*z2 + bias3.repeat(1,4)

        z[z>=1] = 1.
        z[z<1] = 0.

        mse = torch.sum(torch.pow(od - z, 2), dim=1) / n

        index = torch.argmin(mse)
        coordinate = coordinates[index]
        self.TestNetwork(coordinate)

        return mse

    """Prueba la Red Neuronal con los datos de Test"""
    def TestNetwork(self, coordinate):
        w1 = coordinate[0]
        w2 = coordinate[1]
        w3 = coordinate[2]
        w4 = coordinate[3]
        w5 = coordinate[4]
        w6 = coordinate[5]
        bias1 = coordinate[6]
        bias2 = coordinate[7]
        bias3 = coordinate[8]

        x1 = self.GetInputTest()[:,0]
        x2 = self.GetInputTest()[:,1]
        od = self.GetOutputTest()

        n = od.size()[0]

        z1 = x1*w1 + x2*w3 + bias1
        z2 = x1*w2 + x2*w4 + bias2

        #Capa RELU
        z1[z1 < 0] = 0
        z2[z2 < 0] = 0

        z = z1*w5 + z2*w6 + bias3

        z[z>=1] = 1.
        z[z<1] = 0.
        diff = od - z
        error = diff != 0

        pe = torch.sum(error) / float(n)
        print(f'Porcentaje de Precisión: {(1 - pe)*100}')

    """Guarda las coordenadas de la Luciernaga con la mejor puntuación"""
    def SaveNetwork(self, location):
        pass

    """Lee los valores guardados y los carga en el modelo"""
    def LoadNetwork(self, location):
        pass

    """Evalua el resultado de una entrada con la red neuronal cargada"""
    def Evaluate(self, data):
        pass