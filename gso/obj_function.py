import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from gso import GSOParameters
from gso.space import Space

"""Función Objetivo"""
"""Es la función a optimizar usando en el Algoritmo GSO"""
class ObjectiveFunction(object):
    """Clase Padre para todas Funcion Objetivo que se van a optimziar"""

    def __init__(self):
        self.__num_glowworms = 200
        self.__parameters = GSOParameters()
        self.__space = Space(0, 0, 0)
    
    def getNumGlowworms(self):
        return self.__num_glowworms

    def setNumGlowworms(self, numGlowworms):
        self.__num_glowworms = numGlowworms
    
    def getParameters(self):
        return self.__parameters

    def setParameters(self, parameters):
        self.__parameters = parameters
    
    def getSpace(self):
        return self.__space

    def setSpace(self, space):
        self.__space = space

    def PlotSurface(self):
        x = np.linspace(self.getSpace().getDimension(0).getLowerLimit(), self.getSpace().getDimension(0).getUpperLimit(), 100)
        y = np.linspace(self.getSpace().getDimension(1).getLowerLimit(), self.getSpace().getDimension(1).getUpperLimit(), 100)

        X, Y = np.meshgrid(x, y)

        Z = self.PlotCalculate(X,Y)

        fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
        surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,linewidth=0, antialiased=False)

        ax.set_zlim(np.min(Z), np.max(Z))
        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter('{x:.02f}')

        ax.set_xlabel('$X$')
        ax.set_ylabel('$Y$')
        ax.set_zlabel('$Z$')

        fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.show()
