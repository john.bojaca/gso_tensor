from gso.i_obj_function import IObjectiveFunction
import  torch
import numpy as np
from gso.dimension import Dimension
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Bukin"""
"""Mínimo Global => f(x, y) = 0 en (-10, 1)"""
class Bukin(ObjectiveFunction, IObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(0, 0, 0))
        self.getSpace().addDimension(Dimension(0, -15, -5))
        print(f'Creando... {self.getSpace().getDimension(0).__repr__()}')
        self.getSpace().addDimension(Dimension(1, -3, 3))
        print(f'Creando... {self.getSpace().getDimension(1).__repr__()}')

        self.setNumGlowworms(200)
        
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return Bukin.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        x = coordinates[:,0]
        y = coordinates[:,1]

        return 100*torch.sqrt(torch.abs(y - 0.01*torch.pow(x, 2))) + 0.01*torch.abs(x + 10)
    
    def PlotCalculate(self, x, y):
        return 100*np.sqrt(np.abs(y - 0.01*np.power(x, 2))) + 0.01*np.abs(x + 10)