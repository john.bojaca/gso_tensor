import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función RotateEllipsoide"""
"""Mínimo Global => f(x*) = 0, en (0,...,0)"""
class RotateEllipsoide(ObjectiveFunction):
    def __init__(self):      
        dimension = 2
        self.setSpace(Space(dimension, -10.0, 10.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True, max_vision_range=30, ini_vision_range=12))
        
        if dimension == 2:
            self.PlotSurface()

    def __call__(self, coordinates):
        return RotateEllipsoide.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")

        for i in range(0, dimension):
            for j in range(0, i+1):
                first = first + torch.pow(coordinates[:,j], 2)

        return first
    
    def PlotCalculate(self, x, y):
        return 2*np.power(x, 2) + np.power(y, 2)