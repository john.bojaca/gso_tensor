import argparse
import torch

parser = argparse.ArgumentParser()

parser.add_argument("-us", "--UseCase", help = "Use Case Type", required=True, type=int)
parser.add_argument("-ts", "--TechSupported", help = "Technology Supported (LTE/5G or IoT)", required=True, type=int)
parser.add_argument("-g", "--GBR", help = "GBR (GBR or Non-GBR)", required=True,  type=int)
parser.add_argument("-lr", "--LossRate", help = "Packet Loss Rate. Ranges (0 - 0,000001 | 0,000001 - 0,001 | 0,001 - 0,01)", required=True, type=int)
parser.add_argument("-d", "--Delay", help = "Packet Delay. Ranges (0 - 10ms | 10ms - 50ms | 50ms - 60ms | 60ms - 75ms | 75ms - 100ms | 100ms - 150ms | 150ms - 300ms)", required=True, type=int)

UseCases = ["AR/VR/Gaming", "Healthcare", "Industry 4.0", "IoT Devices", "Public Safety/E911", "Smart City & Home", "Smart Transportation", "Smartphone"]
TechsSupported = ["LTE/5G", "IoT (LTE-M, NB-IoT)"]
GBRs = ["GBR", "Non-GBR"]
LossRates = ["0,01", "0,001", "0,000001"]
Delays = ["10ms", "50ms", "60ms", "75ms", "100ms", "150ms", "300ms"]


# Read arguments from command line
args = parser.parse_args()
 
if args.UseCase < 1 or args.UseCase > len(UseCases):
    print(f"User Cases availables are:")
    for i in range(1, len(UseCases)+1):
        print(f"[{i}] {UseCases[i-1]}")
    raise Exception("argument -us/--UseCase: invalid value")

if args.TechSupported < 1 or args.TechSupported > len(TechsSupported):
    print(f"User Cases availables are:")
    for i in range(1, len(TechsSupported)+1):
        print(f"[{i}] {TechsSupported[i-1]}")
    raise Exception("argument -ts/--TechSupported: invalid value")

if args.GBR < 1 or args.GBR > len(GBRs):
    print(f"GBR types availables are:")
    for i in range(1, len(GBRs)+1):
        print(f"[{i}] {GBRs[i-1]}")
    raise Exception("argument -g/--GBR: invalid value")

if args.LossRate < 1 or args.LossRate > len(LossRates):
    print(f"Packet Loss rate availables are:")
    for i in range(1, len(LossRates)+1):
        print(f"[{i}] {LossRates[i-1]}")
    raise Exception("argument -lr/--LossRate: invalid value")

if args.Delay < 1 or args.Delay > len(Delays):
    print(f"User Cases availables are:")
    for i in range(1, len(Delays)+1):
        print(f"[{i}] {Delays[i-1]}")
    raise Exception("argument -d/--Delay: invalid value")

useCaseTensor = torch.zeros(len(UseCases), device="cuda")
useCaseTensor[args.UseCase -1] = 1
print(useCaseTensor)

TechSupportedTensor = torch.zeros(len(TechsSupported), device="cuda")
TechSupportedTensor[args.TechSupported -1] = 1
print(TechSupportedTensor)

GBRTensor = torch.zeros(len(GBRs), device="cuda")
GBRTensor[args.GBR -1] = 1
print(GBRTensor)

LossRateTensor = torch.zeros(len(LossRates), device="cuda")
LossRateTensor[args.LossRate -1] = 1
print(LossRateTensor)

DelayTensor = torch.zeros(len(Delays), device="cuda")
DelayTensor[args.Delay -1] = 1
print(DelayTensor)
#torch.tensor([1.,0.,0.,0.,0.,0.,0.,0.,1.,0.,0.,1.,1.,0.,0.,0.,0.,0.,0.,1.,0.,0.], device="cuda")