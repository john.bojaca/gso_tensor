import nn
import torch
import matplotlib.pyplot as plt
from sklearn import metrics

obj_function = nn.MPLDeepSlice()

location =  'D:\Maestria\Tesis\DataSets\DeepSlice\DeepSlice.pt'
obj_function.LoadNetwork(location)

input = torch.tensor([1.,0.,0.,0.,0.,0.,0.,0.,1.,0.,0.,1.,1.,0.,0.,0.,0.,0.,0.,1.,0.,0.], device="cuda")
print(f'Entradas: {input} - Salida: {obj_function.Evaluate(input)}')

input = torch.tensor([0.,1.,0.,0.,0.,0.,0.,0.,0.,1.,1.,0.,1.,0.,0.,0.,0.,0.,0.,0.,0.,1.], device="cuda")
print(f'Entradas: {input} - Salida: {obj_function.Evaluate(input)}')

input = torch.tensor([0.,0.,0.,0.,0.,0.,1.,0.,0.,1.,0.,1.,0.,0.,1.,1.,0.,0.,0.,0.,0.,0.], device="cuda")
print(f'Entradas: {input} - Salida: {obj_function.Evaluate(input)}')

outputsTrain = obj_function.model(obj_function.dataTrain.x)
outputsTest = obj_function.model(obj_function.dataTest.x)

outputs = torch.cat((outputsTrain, outputsTest))
y = torch.cat((obj_function.dataTrain.y, obj_function.dataTest.y))

correct = (outputs == y).sum()
print(f'Total Accuaracy: {(correct/(obj_function.totalTrain + obj_function.totalTest))*100}')

confusion_matrix = metrics.confusion_matrix(y.cpu(), outputs.cpu())
cm_display = metrics.ConfusionMatrixDisplay(confusion_matrix = confusion_matrix, display_labels = [1, 2, 3])
cm_display.plot()
plt.show()