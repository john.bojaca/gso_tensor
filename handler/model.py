import torch
import torch.nn as nn

class MLPnet(nn.Module):
    def __init__(self, input_dim=22, hidden_dim=23, output_dim=3):
        super(MLPnet, self).__init__()
        self.input_dim = input_dim

        self.linear1 = nn.Linear(input_dim, hidden_dim) 
        self.relu1 = nn.ReLU()
        self.linear2 = nn.Linear(hidden_dim, hidden_dim) 
        self.relu2 = nn.ReLU()
        self.linear4 = nn.Linear(hidden_dim, output_dim)
        self.tanh = nn.Tanh()
        self.softmax = nn.Softmax(dim=1)  

    def forward(self, x):
        x = x.view(-1, self.input_dim)
        z1 = self.linear1(x)
        y1 = self.relu1(z1)
        z2 = self.linear2(y1)
        y2 = self.relu2(z2)
        z4 = self.linear4(y2)
        y4 = self.tanh(z4)
        out = self.softmax(y4)

        return torch.argmax(out, dim=1) + 1