import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función EggHolder"""
"""Mínimo Global => f(x, y) = -959.6407 en (512, 404.2319)"""
class EggHolder(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -550.0, 550.0))
        self.setNumGlowworms(300)
        self.setParameters(GSOParameters(step_size=16, max_vision_range=600, ini_vision_range=80, find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return EggHolder.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        x = coordinates[:,0]
        y = coordinates[:,1]

        return -(y + 47)*torch.sin(torch.sqrt(torch.abs(y + x/2 + 47))) - x*torch.sin(torch.sqrt(torch.abs(x-(y+47))))
    
    def PlotCalculate(self, x, y):
        return -(y + 47)*np.sin(np.sqrt(np.abs(y + x/2 + 47))) - x*np.sin(np.sqrt(np.abs(x-(y+47))))