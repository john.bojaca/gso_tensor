import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Levy"""
"""Mínimo Global => f(x) = 0, en x = (1,1)"""
class Levy(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -3.0, 3.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return Levy.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        x = coordinates[:,0]
        y = coordinates[:,1]

        return torch.pow(torch.sin(3*torch.pi*x), 2) + torch.pow(x - 1, 2)*(1 + torch.pow(torch.sin(3*torch.pi*y), 2)) + torch.pow(y -1, 2)*(1 + torch.pow(torch.sin(2*torch.pi*y), 2))
    
    def PlotCalculate(self, x, y):
        return np.power(np.sin(3*np.pi*x), 2) + np.power(x - 1, 2)*(1 + np.power(np.sin(3*np.pi*y), 2)) + np.power(y -1, 2)*(1 + np.power(np.sin(2*np.pi*y), 2))