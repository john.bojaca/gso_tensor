import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Trid"""
"""Mínimo Global => f(x) = -2, en x = (2, 2) -> i(d+1-i)"""
class Trid(ObjectiveFunction):
    def __init__(self):       
        dimension = 4
        self.setSpace(Space(dimension, -np.power(dimension, 2), np.power(dimension, 2))) # Hipercubo de (-d^2, d^2)
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True, max_vision_range=30, ini_vision_range=12))
        
        if dimension == 2:
            self.PlotSurface()

    def __call__(self, coordinates):
        return Trid.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")
        second = torch.zeros(glowworms, dtype=torch.float, device="cuda")

        for i in range(0, dimension):
            first = first + torch.pow(coordinates[:,i] - 1, 2)
        
        for i in range(1, dimension):
            second = second + coordinates[:,i]*coordinates[:,i-1]
        
        return first - second
    
    def PlotCalculate(self, x, y):
        return np.power((x-1), 2) + np.power((y-1), 2) - x*y