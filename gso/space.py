from gso.dimension import Dimension

"""Espacio de N-Dimensiones donde se moveran las luciernagas"""
class Space(object):

    """Genera un Espacio de N-Dimensiones cada una con el mismo Límite inferior y Superior"""
    def __init__(self, num_dimensions, lower_limit, upper_limit):
        self.__dimensions = [
            Dimension(i, lower_limit, upper_limit) for i in range(num_dimensions)   
        ]
    
    """Devuelve el número de Dimensiones del Espacio"""
    def getNumDimension(self):
        return len(self.__dimensions)

    """Devuelve una Dimension ingresando su ID"""
    def getDimension(self, num_dimension):
        return self.__dimensions[num_dimension]
    
    def addDimension(self, dimension):
        return self.__dimensions.append(dimension)