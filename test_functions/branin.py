from gso.i_obj_function import IObjectiveFunction
import  torch
import numpy as np
from gso.dimension import Dimension
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Branin"""
"""Mínimo Global => f(x, y) = 0.397887, en x = (-pi, 12.275), (pi, 2.275), (9.42478, 2.475)"""
class Branin(ObjectiveFunction, IObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(0, 0, 0))

        self.getSpace().addDimension(Dimension(0, -5, 10))
        print(f'Creando... {self.getSpace().getDimension(0).__repr__()}')
        self.getSpace().addDimension(Dimension(1, 0, 15))
        print(f'Creando... {self.getSpace().getDimension(1).__repr__()}')

        self.setNumGlowworms(200)
        
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return Branin.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        x = coordinates[:,0]
        y = coordinates[:,1]

        a = 1
        b = 5.1 / (4*np.power(np.pi, 2))
        c = 5 / np.pi
        r = 6
        s = 10
        t = 1 / 8*np.pi

        return a*torch.pow(y - b*torch.pow(x, 2) + c*x - r, 2) + s*(1 - t)*torch.cos(x) + s
    
    def PlotCalculate(self, x, y):
        self.a = 1
        self.b = 5.1 / (4*np.power(np.pi, 2))
        self.c = 5 / np.pi
        self.r = 6
        self.s = 10
        self.t = 1 / 8*np.pi

        return self.a*np.power(y - self.b*np.power(x, 2) + self.c*x - self.r, 2) + self.s*(1 - self.t)*np.cos(x) + self.s