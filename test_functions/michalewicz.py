from gso.gso_parameters import GSOParameters
from gso.space import Space
import  torch
import numpy as np
from gso.obj_function import ObjectiveFunction

"""Función Michalewicz"""
"""Mínimo Global => f(x, y) = -1.8013, en x = (2.2, 1.57)"""
class Michalewicz(ObjectiveFunction):
    def __init__(self):
        dimension = 2
        self.setSpace(Space(dimension, 0, np.pi))
        
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        
        if dimension == 2:
            self.PlotSurface()

    def __call__(self, coordinates):
        return Michalewicz.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        m = 10      
        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")

        for i in range(0, dimension):
            first = first + torch.sin(coordinates[:,i])*torch.pow(torch.sin(((i+1)*torch.pow(coordinates[:,i], 2))/torch.pi), 2*m)

        ##return  -1*((torch.sin(x)*torch.pow(torch.sin((torch.pow(x, 2)) / torch.pi), 20)) + (torch.sin(y) * torch.pow(torch.sin((2*torch.pow(y,2)) / torch.pi), 20)))
        return -first
    
    def PlotCalculate(self, x, y):
        return -1*((np.sin(x)*np.power(np.sin((np.power(x, 2)) / np.pi), 20)) + (np.sin(y) * np.power(np.sin((2*np.power(y,2)) / np.pi), 20)))