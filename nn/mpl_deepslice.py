from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space
from nn.dataset_deepslice import DSDeepSlice
from nn.neural_network import NeuralNetwork
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torch.nn as nn
import torch
import pandas as pd
from sklearn.model_selection import train_test_split

class MPLDeepSlice(ObjectiveFunction, NeuralNetwork):
    def __init__(self):
        self.LoadData()
        self.CreateModel()

        self.setNumGlowworms(200)

        self.setParameters(GSOParameters(ini_vision_range=12, max_vision_range=30, step_size=0.4, max_neighbors=5, find_min=True))
        self.setSpace(Space(self.GetVariables(), -1.0, 1.0))
        print(f'Dimensiones: {self.GetVariables()}')
        print(f'Modelo: {self.model}')

    """Carga la información para entrenamiento y test para Red Neuronal"""
    def LoadData(self):
        #file_name = 'D:\\Maestria\\Tesis\\DataSets\\DeepSlice\\filtereddata.txt'
        file_name = 'D:\\Maestria\\Tesis\\DataSets\\DeepSlice\\filtereddataClean2.csv'
        print(f'Leyendo información del archivo {file_name}')
        df = pd.read_csv(file_name)
        self.train, self.test = train_test_split(df, test_size=0.3)

        self.LoadDataTraining()
        self.LoadDataTest()

    """Carga la información para el entrenamiento de la Red neuronal"""
    def LoadDataTraining(self):
        self.dataTrain = DSDeepSlice(self.train)
        self.totalTrain = self.dataTrain.__len__()
        print(f"Datos de Entrenamiento cargados ... Número de registros {self.totalTrain}")

    """Carga la información para las pruebas de la red neuronal"""
    def LoadDataTest(self):
        self.dataTest = DSDeepSlice(self.test)
        self.totalTest = self.dataTest.__len__()
        print(f"Datos de Prueba cargados ... Número de registros {self.totalTest}")

    """Retorna los datos de entrada para el entrenamiento"""
    def GetInputTraining(self):
        pass

    """Retorna los datos de salida para el entrenamiento"""
    def GetOutputTraining(self):
        pass

    """Retorna los datos de entrada para las pruebas"""
    def GetInputTest(self):
        pass

    """Retorna los datos de salida para las pruebas"""
    def GetOutputTest(self):
        pass

    """Crea el modelo de la red neuronal"""
    def CreateModel(self):
        input_dim  = 22
        hidden_dim = 23
        output_dim = 3

        self.model = MLPnet(input_dim, hidden_dim, output_dim)
        self.model = self.model.cuda()

        self.dimensions = 0
        for param_tensor in self.model.state_dict():
            self.dimensions += self.model.state_dict()[param_tensor].nelement()
    
    """Retornal el número de variables de la Red Neuronal"""
    def GetVariables(self):
        return self.dimensions

    """Llamado del Algoritmo GSO con las coordenadas de las Luciernagas"""
    def __call__(self, coordinates):
        return self.TrainNetwork(coordinates)

    """Entrena la Red Neuronal con el Algotirmo GSO"""
    def TrainNetwork(self, coordinates):
        num_glowworms = coordinates.size(dim=0)
        values = torch.zeros(num_glowworms, device="cuda")
        
        self.model.eval()
        with torch.no_grad():

            for glowworm in range(num_glowworms):
                self.___setCoordinatesInModel___(coordinates[glowworm])
                outputs = self.model(self.dataTrain.x)

                mse = torch.sum(torch.pow(self.dataTrain.y - outputs, 2)) / self.totalTrain
                values[glowworm] = mse
            
            index = torch.argmin(values)
            best = coordinates[index]
            self.___setCoordinatesInModel___(best)

            outputs = self.model(self.dataTest.x)

            correct = (outputs == self.dataTest.y).sum()
            print(f'Accuaracy: {(correct/self.totalTest)*100} - Mejor MSE: {values[index]}')

        return values
    
    def ___setCoordinatesInModel___(self, coordinate):
        sd = self.model.state_dict()
        init_index = 0
        
        for param_tensor in sd:
            size_layer = sd[param_tensor].nelement()
            param = coordinate[init_index : init_index + size_layer]
            param = param.reshape(self.model.state_dict()[param_tensor].size())
            sd[param_tensor] = param
            init_index = init_index + size_layer
        
        self.model.load_state_dict(sd)

    """Prueba la Red Neuronal con los datos de Test"""
    def TestNetwork(self, coordinate):
        self.___setCoordinatesInModel___(coordinate)
        location =  'D:\Maestria\Tesis\DataSets\DeepSlice\DeepSlice.pt'
        self.SaveNetwork(location)

        outputsTrain = self.model(self.dataTrain.x)
        correctTrain = (outputsTrain == self.dataTrain.y).sum()

        outputsTest = self.model(self.dataTest.x)
        correctTest = (outputsTest == self.dataTest.y).sum()
        
        print(f'Total Accuaracy: {((correctTrain + correctTest)/(self.totalTrain + self.totalTest))*100}')

    """Guarda las coordenadas de la Luciernaga con la mejor puntuación"""
    def SaveNetwork(self, location):
        torch.save(self.model.state_dict(), location)

    """Lee los valores guardados y los carga en el modelo"""
    def LoadNetwork(self, location):
        self.model.load_state_dict(torch.load(location))
        #self.model.state_dict = torch.load(self.model, location)
        self.model.eval()

    """Evalua el resultado de una entrada con la red neuronal cargada"""
    def Evaluate(self, data):
        return self.model(data)

class MLPnet(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim):
        super(MLPnet, self).__init__()
        self.input_dim = input_dim

        self.linear1 = nn.Linear(input_dim, hidden_dim) 
        self.relu1 = nn.ReLU()
        self.linear2 = nn.Linear(hidden_dim, hidden_dim) 
        self.relu2 = nn.ReLU()
        #self.linear3 = nn.Linear(hidden_dim, hidden_dim) 
        #self.relu3 = nn.ReLU()
        self.linear4 = nn.Linear(hidden_dim, output_dim)
        self.tanh = nn.Tanh()
        self.softmax = nn.Softmax(dim=1)  

    def forward(self, x):
        x = x.view(-1, self.input_dim)
        z1 = self.linear1(x)
        y1 = self.relu1(z1)
        z2 = self.linear2(y1)
        y2 = self.relu2(z2)
        #z3 = self.linear3(y2)
        #y3 = self.relu3(z3)
        z4 = self.linear4(y2)
        y4 = self.tanh(z4)
        out = self.softmax(y4)

        return torch.argmax(out, dim=1) + 1
    
    def name(self):
        return "MLP"