import numpy as np
import torch
from scipy.spatial import distance

"""Un enjambre de Luciernagas"""
class Swarm(object):

    """Constructor de la Clase Enjambre de Luciernagas"""
    def __init__(self, obj_function):
        self.__obj_function = obj_function

        """Inicializa la Lucerferina y el Rango de Visión con los valores iniciales"""
        self.__luciferin = torch.ones(self.__obj_function.getNumGlowworms(), dtype=torch.float, device="cuda")*self.__obj_function.getParameters().getIniLuciferin()
        self.__vision_range = torch.ones(self.__obj_function.getNumGlowworms(), dtype=torch.float, device="cuda")*self.__obj_function.getParameters().getIniVisionRange()

        """Inicializa las Coordenadas de las Luciernagas de manera aleatoria"""
        self.__coordinates = torch.rand((self.__obj_function.getNumGlowworms(), self.__obj_function.getSpace().getNumDimension()), dtype=torch.float, device="cuda")
        for i in range(self.__obj_function.getSpace().getNumDimension()):
            #print(f'Dimension {i}')
            #print(f'Antes: {self.__coordinates[:,i]}')
            self.__coordinates[:,i] = self.__coordinates[:,i]*(self.__obj_function.getSpace().getDimension(i).getUpperLimit() - self.__obj_function.getSpace().getDimension(i).getLowerLimit()) + self.__obj_function.getSpace().getDimension(i).getLowerLimit()
            #print(f'Despues: {self.__coordinates[:,i]}')

    def getScoring(self):
        return self.__scoring
    
    def getCoordinates(self):
        return self.__coordinates
    
    def getDistances(self):
        return self.__distances

    """Actualiza los valores de la Lucerferina"""
    def update_luciferin(self):
        self.__InfoLog("* Actualizando la Lucerferina")
        self.__scoring = self.__obj_function(self.__coordinates)
        self.__luciferin = (1.0 - self.__obj_function.getParameters().getRho()) * self.__luciferin + self.__obj_function.getParameters().getGamma() * self.__scoring
        return self.__luciferin
    
    """Encuentra la probabilidad de movimiento de cada luciernaga y se mueve siguiente el Algoritmo GSO"""
    def movement_phase(self):
        self.__InfoLog("* Buscando las Luciernagas vecinas")
        self.__search_neighbors()
        
        self.__InfoLog("* Calculando la Probabilidad de Movimiento")
        self.__compute_probability_moving_toward_neighbor()

        self.__InfoLog("* Seleccionando las Luciernagas vecinas para efectuar el movimientos")
        self.__select_random_neighbor()

        self.__InfoLog("* Efectuando los movimientos hacia las luciernagas vecinas")
        self.__move()
    
    """Para cada luciernaga, busca las luciernagas vecinas"""
    """1. Determina si la Lucerferina de las Lucierngas es Mayor con respecto a las otras"""
    """2. Determina si la distancia entre las luciernangas esta dentro del rango de vision de cada Luciernaga"""
    """Si cumple estas dos condiciones se considera que una Luciernga es Vecina de otra"""
    def __search_neighbors(self):
        self.__neighbors = torch.zeros((self.__obj_function.getNumGlowworms(), self.__obj_function.getNumGlowworms()), dtype=torch.bool, device="cuda")
        in_range = torch.zeros((self.__obj_function.getNumGlowworms(), self.__obj_function.getNumGlowworms()), dtype=torch.bool, device="cuda")
        
        self.__InfoLog("*** Calculando las Distancias entre Luciernagas")
        distances = torch.cdist(self.__coordinates, self.__coordinates, p=2)
        self.__distances = distances

        self.__InfoLog("*** Compara la intesidad de la Lucerferina entre Luciernagas")
        luciferin_tensor = self.__luciferin.repeat(self.__obj_function.getNumGlowworms(), 1)
        if self.__obj_function.getParameters().isFindMin():
            self.__neighbors = torch.transpose(luciferin_tensor, 0, 1) > luciferin_tensor
        else:
            self.__neighbors = torch.transpose(luciferin_tensor, 0, 1) < luciferin_tensor
        
        self.__InfoLog("*** Buscando las luciernagas que estan dentro de los rangos de visión")        
        vision_range_tensor = self.__vision_range.repeat(self.__obj_function.getNumGlowworms(), 1)
        in_range = distances < torch.transpose(vision_range_tensor, 0, 1)

        self.__InfoLog("*** Buscando los vecinos dentro del Rango de Visión y la Lucerferina")
        self.__neighbors = torch.logical_and(self.__neighbors, in_range)
    
    """Calcula la probabilidad de las Luciernagas de moverse hacia sus vecinas"""
    def __compute_probability_moving_toward_neighbor(self):
        self.__probabilities = torch.zeros((self.__obj_function.getNumGlowworms(), self.__obj_function.getNumGlowworms()), dtype=torch.float, device="cuda")

        infinity = torch.isinf(self.__luciferin)
        infinity_tensor = infinity.repeat(self.__obj_function.getNumGlowworms(), 1)
        replace_infinity = torch.logical_and(infinity_tensor, torch.transpose(infinity_tensor, 0, 1))

        self.__InfoLog("*** Calculando la diferencia de Lucerferina de las Luciernagas Vecinas")
        luciferin_tensor = self.__luciferin.repeat(self.__obj_function.getNumGlowworms(), 1)

        difference = luciferin_tensor - torch.transpose(luciferin_tensor, 0, 1)
        
        difference[replace_infinity] = 0
        if self.__obj_function.getParameters().isFindMin():
            difference[difference > 0] = 0
        else:
            difference[difference < 0] = 0
        difference[self.__neighbors == False] = 0

        if self.__obj_function.getParameters().isFindMin():
            difference = -1*difference
        
        self.__InfoLog("*** Calculando la Suma total de las diferencias de las Luciernagas vecinas")
        sum = torch.sum(difference, dim=1)

        self.__InfoLog("*** Calculando la probabilidad de Movimiento de hacia cada una de las Luciernagas vecinas")
        for glowworm in range(self.__obj_function.getNumGlowworms()):
            if sum[glowworm] > 0:
                self.__probabilities[glowworm] = torch.div(difference[glowworm], sum[glowworm])
            if sum[glowworm] == torch.inf:
                self.__probabilities[glowworm] = 1

    """Selecciona una luciernaga vecina usando la probalidad de movimiento calculada para cada luciernaga"""
    def __select_random_neighbor(self):
        self.__neighbors_selected = torch.zeros(self.__obj_function.getNumGlowworms(), dtype=torch.int, device="cuda")
        
        self.__InfoLog("*** Generando tensor de número aleatorios")
        random_numbers = torch.rand(self.__obj_function.getNumGlowworms(), device="cuda")
        random_tensor = torch.transpose(random_numbers.repeat(self.__obj_function.getNumGlowworms(), 1), 0, 1)
        
        self.__InfoLog("*** Calculando la suma acumulada de las probabilidades")
        cumsum = torch.cumsum(self.__probabilities, dim=1)

        self.__InfoLog("*** Buscando las Luciernadas Vecinas que probabilidad más alto del valor aleatorio")
        selected = cumsum > random_tensor

        self.__InfoLog("*** Seleccionado las Luciernagas Vecinas para efectuar el movimiento")
        for glowworm in range(self.__obj_function.getNumGlowworms()):
            index = (selected[glowworm] == True).nonzero()
            
            if (index.size(dim=0) > 0):
                self.__neighbors_selected[glowworm] = index[0]
            else:
                self.__neighbors_selected[glowworm] = glowworm
    
    """Efectua el cambio de coordenadas de cada Luciernaga hacia su vecina (Movimiento)"""
    def __move(self):
        routes = torch.zeros((self.__obj_function.getNumGlowworms(), self.__obj_function.getSpace().getNumDimension()), dtype=torch.float, device="cuda")

        self.__InfoLog("*** Generando coordenadas para las rutas")
        for glowworm in range(self.__obj_function.getNumGlowworms()):
            index = self.__neighbors_selected[glowworm]
            routes[glowworm] = self.__coordinates[index]

        self.__InfoLog("*** Calculando la diferencia entre las coordenadas de origen y destino")
        diff = routes - self.__coordinates

        self.__InfoLog("*** Calculando la Norma de las Coordenadas")
        norm = torch.sqrt(torch.sum(torch.pow(diff, 2), dim=1))
        norm = torch.transpose(norm.repeat(self.__obj_function.getSpace().getNumDimension(), 1), 0, 1)

        self.__InfoLog("*** Calculando el Delta del Movimiento")
        delta = torch.zeros((self.__obj_function.getNumGlowworms(), self.__obj_function.getSpace().getNumDimension()), dtype=torch.float, device="cuda")
        mask = (norm != 0)
        delta[mask] = diff[mask]*(self.__obj_function.getParameters().getStepSize() / norm[mask])

        self.__InfoLog("*** Calculando las nuevas Coordenadas de las Luciernagas")
        self.__coordinates = self.__coordinates + delta
    
    """Calcula y actualiza el Rango de Visión de las Luciernagas"""
    def update_vision_range(self):
        num_neighbors = torch.sum( self.__neighbors, dim=1)
        max_vision_tensor = torch.ones(self.__obj_function.getNumGlowworms(), dtype=torch.float, device="cuda")*self.__obj_function.getParameters().getMaxVisionRange()

        self.__InfoLog("*** Calculando los nuevos Rangos de Visión de las Luciernagas")
        self.__vision_range = torch.maximum(
            torch.zeros(self.__obj_function.getNumGlowworms(), dtype=torch.float, device="cuda"),
            torch.minimum(
                max_vision_tensor,
                self.__vision_range + self.__obj_function.getParameters().getBeta() * (self.__obj_function.getParameters().getMaxNeighbors() - num_neighbors)
            )
        )

    """Imprime los mensajes dependiendo si el verbose esta activo o no"""
    def __InfoLog(self, message):
        if self.__obj_function.getParameters().isVerbose():
            print(message)
