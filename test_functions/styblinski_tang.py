import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Styblinski-Tang"""
"""Mínimo Global => f(x, y) = -39.16599*d, en x = (-2.903534,...,-2.903534)"""
class StyblinskiTang(ObjectiveFunction):
    def __init__(self):        
        dimension = 2
        self.setSpace(Space(dimension, -5.0, 5.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        
        if dimension == 2:
            self.PlotSurface()

    def __call__(self, coordinates):
        return StyblinskiTang.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")

        for i in range(0, dimension):
            first = first + (torch.pow(coordinates[:,i], 4) - 16*torch.pow(coordinates[:,i], 2) + 5*coordinates[:,i])
        
        return 0.5*first
    
    def PlotCalculate(self, x, y):
        return 0.5*((np.power(x, 4) - 16*np.power(x, 2) + 5*x) + (np.power(y, 4) - 16*np.power(y, 2) + 5*y))