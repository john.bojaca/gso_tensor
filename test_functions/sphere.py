import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Sphere"""
"""Mínimo Global => f(x*) = 0, en x = (0,...,0)"""
class Sphere(ObjectiveFunction):
    def __init__(self):        
        dimension = 2
        self.setSpace(Space(dimension, -5.0, 5.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        
        if dimension == 2:
            self.PlotSurface()

    def __call__(self, coordinates):
        return Sphere.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")

        for i in range(0, dimension):
            first = first + torch.pow(coordinates[:,i], 2)

        return first
    
    def PlotCalculate(self, x, y):
        return np.power(x, 2) + np.power(y, 2)