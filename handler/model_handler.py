import json
import torch
from ts.torch_handler.base_handler import BaseHandler
from ts.utils.util import PredictionException

class DeepSlice(BaseHandler):
    
    def __init__(self):
        self.model = None

    def initialize(self, context):
        super().initialize(context)

    def preprocess(self, data):
        jsonStr = data[0].get("data") or data[0].get("body")
        y = json.loads(json.dumps(jsonStr))

        caseType = int(y["CaseType"])
        technology = int(y["Technology"])
        gbr = int(y["GBR"])
        lossRate = int(y["LossRate"])
        delay = int(y["Delay"])

        if (caseType < 1 or caseType > 8):
            raise PredictionException("CaseType range between 1 and 8", 400)
        
        if (technology < 1 or technology > 2):
            raise PredictionException("Technology range between 1 and 2", 400)
        
        if (gbr < 1 or gbr > 2):
            raise PredictionException("GBR range between 1 and 2", 400)
        
        if (lossRate < 1 or lossRate > 3):
            raise PredictionException("LossRate range between 1 and 3", 400)
        
        if (delay < 1 or delay > 7):
            raise PredictionException("Delay range between 1 and 7", 400)
        
        ret = torch.zeros(22)
        ret[caseType - 1] = 1
        ret[technology + 7] = 1
        ret[gbr + 9] = 1
        ret[lossRate + 11] = 1
        ret[delay + 14] = 1

        print(ret)

        return ret

    def inference(self, data, *args, **kwargs):
        self.model.eval()
        with torch.no_grad():
            marshalled_data = data.to(self.device)
            results = self.model(marshalled_data, *args, **kwargs)
        return results

    def postprocess(self, data):
        ret = {}
        ret['id'] = data.item()
        
        if data == 1:
            ret['name'] = 'eMMB'
        elif data == 2:
            ret['name'] = 'URLLC'
        elif data == 3:
            ret['name'] = 'mMTC'
        else:
            raise PredictionException("The predictions is not avalaible", 500)
        
        list = []
        list.append(json.loads(json.dumps(ret)))
        return list

    def handle(self, data, context):
        self.context = context

        data = self.preprocess(data)
        data = self.inference(data)
        data = self.postprocess(data)
        return data