"""Representa las caracsteristicas de una dimension"""
class Dimension(object):

    """Crea una dimension con limites inferior y superior"""
    def __init__(self, id, lower_limit, upper_limit):
        self.__id = id
        self.__lower_limit = lower_limit
        self.__upper_limit = upper_limit
    
    def getId(self):
        return self.__id
    
    def getLowerLimit(self):
        return self.__lower_limit
    
    def getUpperLimit(self):
        return self.__upper_limit

    def __repr__(self):
        return "Dimension %s. Límites [%s, %s]" % (self.__id, self.__lower_limit, self.__upper_limit)