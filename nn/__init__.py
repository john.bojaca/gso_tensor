# __init__.py
from .neural_network import NeuralNetwork
from .simple_mpl import SimpleMPL
from .mpl import MPL
from .mpl_deepslice import MPLDeepSlice
from .simple_perceptron import SimplePerceptron