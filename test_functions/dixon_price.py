from gso.i_obj_function import IObjectiveFunction
import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función DixonPrice"""
"""Mínimo Global => f(x, y) = 0 en 2^(-(2^i-2)/2^i)"""
class DixonPrice(ObjectiveFunction, IObjectiveFunction):
    def __init__(self):
        dimension = 2
        self.setSpace(Space(dimension, -10.0, 10.0))

        self.setNumGlowworms(200)

        self.setParameters(GSOParameters(find_min=True))

        if dimension == 2:
            self.PlotSurface()
        
        for i in range(1, dimension+1):
            min = np.power(2, -(np.power(2, i)-2)/np.power(2,i))
            print(f"Mínimo {min} en la dimension {i}")

    def __call__(self, coordinates):
        return DixonPrice.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]
        x = coordinates[:,0]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")

        for i in range(1, dimension):
            first = first + i*torch.pow(2*torch.pow(coordinates[:,i], 2) - coordinates[:,i-1], 2)

        return  torch.pow(x - 1, 2) + first
    
    def PlotCalculate(self, x, y):
        return np.power(x - 1, 2) + 2*np.power(2*np.power(y, 2) - x, 2)