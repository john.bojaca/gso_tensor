import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función SixHump"""
"""Mínimo Global => f(x, y) = -1.0316 en (0.0989, -0.7126), (-0.0989, 0.7126)"""
class SixHump(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -3.0, 3.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return SixHump.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        x = coordinates[:,0]
        y = coordinates[:,1]

        return (4 - 2.1*torch.pow(x, 2) + torch.pow(x, 4)/3)*torch.pow(x, 2) + x*y + (-4 + 4*torch.pow(y, 2))*torch.pow(y, 2)
    
    def PlotCalculate(self, x, y):
        return (4 - 2.1*np.power(x, 2) + np.power(x, 4)/3)*np.power(x, 2) + x*y + (-4 + 4*np.power(y, 2))*np.power(y, 2)