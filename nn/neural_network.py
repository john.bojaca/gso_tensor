from abc import abstractmethod
from abc import ABCMeta

class NeuralNetwork(metaclass=ABCMeta):
    
    """Carga la información para entrenamiento y test para Red Neuronal"""
    @abstractmethod
    def LoadData(self):
        pass

    """Carga la información para el entrenamiento de la Red neuronal"""
    @abstractmethod
    def LoadDataTraining(self):
        pass

    """Carga la información para las pruebas de la red neuronal"""
    @abstractmethod
    def LoadDataTest(self):
        pass

    """Retorna los datos de entrada para el entrenamiento"""
    @abstractmethod
    def GetInputTraining(self):
        pass

    """Retorna los datos de salida para el entrenamiento"""
    @abstractmethod
    def GetOutputTraining(self):
        pass

    """Retorna los datos de entrada para las pruebas"""
    @abstractmethod
    def GetInputTest(self):
        pass

    """Retorna los datos de salida para las pruebas"""
    @abstractmethod
    def GetOutputTest(self):
        pass

    """Crea el modelo de la red neuronal"""
    @abstractmethod
    def CreateModel(self):
        pass

    """Retornal el número de variables de la Red Neuronal"""
    @abstractmethod
    def GetVariables(self):
        pass

    """Entrena la Red Neuronal con el Algotirmo GSO"""
    @abstractmethod
    def TrainNetwork(self):
        pass

    """Prueba la Red Neuronal con los datos de Test"""
    @abstractmethod
    def TestNetwork(self, coordinate):
        pass

    """Guarda las coordenadas de la Luciernaga con la mejor puntuación"""
    @abstractmethod
    def SaveNetwork(self, location):
        pass

    """Lee los valores guardados y los carga en el modelo"""
    @abstractmethod
    def LoadNetwork(self, location):
        pass

    """Evalua el resultado de una entrada con la red neuronal cargada"""
    @abstractmethod
    def Evaluate(self, data):
        pass