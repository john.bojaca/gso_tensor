import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Suma de la diferencia de potencias"""
"""Mínimo Global => f(x*) = 0, en (0,...,0)"""
class SDPF(ObjectiveFunction):
    def __init__(self):        
        dimension = 2
        self.setSpace(Space(dimension, -1.0, 1.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        
        if dimension == 2:
            self.PlotSurface()

    def __call__(self, coordinates):
        return SDPF.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")

        for i in range(0, dimension):
            first = first + torch.pow(torch.abs(coordinates[:,i]), i+2)

        return first
    
    def PlotCalculate(self, x, y):
        return np.power(np.abs(x), 2) + np.power(np.abs(y), 3)