import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Rosenbrock"""
"""F(x*) = 0 en (1,...,1)"""
class Rosenbrock(ObjectiveFunction):
    def __init__(self):       
        dimension = 2
        self.setSpace(Space(dimension, -3.0, 3.0))
        
        self.setNumGlowworms(500)
        self.setParameters(GSOParameters(find_min=True, max_vision_range=10))
        
        if dimension == 2:
            self.PlotSurface()

    def __call__(self, coordinates):
        return Rosenbrock.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")

        for i in range(0, dimension-1):
            first = first + (100*torch.pow(coordinates[:,i+1] - torch.pow(coordinates[:,i], 2), 2) + torch.pow(coordinates[:,i] - 1, 2))

        return first
    
    def PlotCalculate(self, x, y):
        return 100*np.power((y - np.power(x, 2)), 2) + np.power((x-1), 2)