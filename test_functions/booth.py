from gso.i_obj_function import IObjectiveFunction
import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Booth"""
"""Mínimo Global => f(x, y) = 0 en (1, 3)"""
class Booth(ObjectiveFunction, IObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -10.0, 10.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

        print("Salidas: 1.0, 3.0")

    def __call__(self, coordinates):
        return Booth.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        x = coordinates[:,0]
        y = coordinates[:,1]

        return torch.pow((x + 2*y - 7), 2) + torch.pow((2*x + y - 5), 2)
    
    def PlotCalculate(self, x, y):
        return np.power((x + 2*y - 7), 2) + np.power((2*x + y - 5), 2)