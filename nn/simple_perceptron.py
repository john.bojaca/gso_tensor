from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space
from nn.neural_network import NeuralNetwork
import torch

class SimplePerceptron(ObjectiveFunction, NeuralNetwork):
    def __init__(self):
        self.LoadData()
        self.CreateModel()

        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        self.setSpace(Space(self.GetVariables(), 0., 1.))

    """Carga la información para entrenamiento y test para Red Neuronal"""
    def LoadData(self):
        #self.data = torch.tensor([[0., 0., 0.], [0., 1., 0.], [1., 0., 0.], [1., 1., 1.]], device="cuda") ## Compuerta AND
        #self.data = torch.tensor([[0., 0., 0.], [0., 1., 1.], [1., 0., 1.], [1., 1., 1.]], device="cuda") ## Compuerta OR
        self.data = torch.tensor([[0., 0., 0.], [0., 1., 1.], [1., 0., 1.], [1., 1., 0.]], device="cuda") ## Compuerta XOR
        print(f"Datos Cargados ... Número de registros {self.data.size()}")

        self.LoadDataTraining()
        self.LoadDataTest()

    """Carga la información para el entrenamiento de la Red neuronal"""
    def LoadDataTraining(self):
        self.dataTrain = self.data
        print(f"Datos de Entrenamiento cargados ... Número de registros {self.dataTrain.size()}")

    """Carga la información para las pruebas de la red neuronal"""
    def LoadDataTest(self):
        self.dataTest = self.data
        print(f"Datos de Prueba cargados ... Número de registros {self.dataTest.size()}")

    """Retorna los datos de entrada para el entrenamiento"""
    def GetInputTraining(self):
        return self.dataTrain[:,:2]

    """Retorna los datos de salida para el entrenamiento"""
    def GetOutputTraining(self):
        return self.dataTrain[:,2]

    """Retorna los datos de entrada para las pruebas"""
    def GetInputTest(self):
        return self.dataTrain[:,:2]

    """Retorna los datos de salida para las pruebas"""
    def GetOutputTest(self):
        return self.dataTrain[:,2]

    """Crea el modelo de la red neuronal"""
    def CreateModel(self):
        print("Esta Red Neuronal crea el modelo en el entrenamiento")
    
    """Retornal el número de variables de la Red Neuronal"""
    def GetVariables(self):
        return 3

    """Llamado del Algoritmo GSO con las coordenadas de las Luciernagas"""
    def __call__(self, coordinates):
        return self.TrainNetwork(coordinates)

    """Entrena la Red Neuronal con el Algotirmo GSO"""
    def TrainNetwork(self, coordinates):
        w1 = coordinates[:,0]
        w2 = coordinates[:,1]
        bias = coordinates[:,2]
        
        w1 = w1.reshape(w1.size()[0], 1)
        w2 = w2.reshape(w2.size()[0], 1)
        bias = bias.reshape(bias.size()[0], 1)

        x1 = self.GetInputTraining()[:,0]
        x2 = self.GetInputTraining()[:,1]
        od = self.GetOutputTraining()

        n = od.size()[0]

        x1 = x1.reshape(1, x1.size()[0])
        x2 = x2.reshape(1, x2.size()[0])
        od = od.reshape(1, od.size()[0])

        z = w1@x1 + w2@x2 + bias

        z[z>=1] = 1
        z[z<1] = 0

        mse = torch.sum(torch.pow(od - z, 2), dim=1) / n

        index = torch.argmin(mse)
        coordinate = coordinates[index]
        self.TestNetwork(coordinate)

        return mse

    """Prueba la Red Neuronal con los datos de Test"""
    def TestNetwork(self, coordinate):
        w1 = coordinate[0]
        w2 = coordinate[1]
        bias = coordinate[2]

        x1 = self.GetInputTest()[:,0]
        x2 = self.GetInputTest()[:,1]
        od = self.GetOutputTest()

        n = od.size()[0]

        z = x1*w1 + x2*w2 + bias

        z[z>=1] = 1
        z[z<1] = 0
        diff = od - z
        error = diff != 0

        pe = torch.sum(error) / float(n)
        print(f'Porcentaje de Precisión: {(1 - pe)*100}')

    """Guarda las coordenadas de la Luciernaga con la mejor puntuación"""
    def SaveNetwork(self, location):
        pass

    """Lee los valores guardados y los carga en el modelo"""
    def LoadNetwork(self, location):
        pass

    """Evalua el resultado de una entrada con la red neuronal cargada"""
    def Evaluate(self, data):
        pass