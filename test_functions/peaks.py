import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Peaks"""
class Peaks(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -3.0, 3.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return Peaks.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        x = coordinates[:,0]
        y = coordinates[:,1]
        
        return  3*torch.pow(1-x, 2)*torch.exp(-torch.pow(x, 2) - torch.pow(y+1, 2)) - 10*(x/5 - torch.pow(x, 3) - torch.pow(y,5))*torch.exp(-torch.pow(x,2)-torch.pow(y,2)) - 1/3*torch.exp(-torch.pow(x+1, 2) - torch.pow(y,2))
    
    def PlotCalculate(self, x, y):
        return 3*np.power(1-x, 2)*np.exp(-np.power(x, 2) - np.power(y+1, 2)) - 10*(x/5 - np.power(x, 3) - np.power(y,5))*np.exp(-np.power(x,2)-np.power(y,2)) - 1/3*np.exp(-np.power(x+1, 2) - np.power(y,2))
