from gso.i_obj_function import IObjectiveFunction
import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Bohachevsky"""
"""Mínimo Global => f(x, y) = 0 en (0, 0)"""
class Bohachevsky(ObjectiveFunction, IObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -10, 10))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

        print("Salidas: 0.0, 0.0")

    def __call__(self, coordinates):
        return Bohachevsky.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        x = coordinates[:,0]
        y = coordinates[:,1]

        return torch.pow(x,2) + 2*(torch.pow(y,2)) - 0.3*torch.cos(3*torch.pi*x) - 0.4*torch.cos(4*torch.pi*y) + 0.7
    
    def PlotCalculate(self, x, y):
        return x**2 + 2*(y**2) - 0.3*np.cos(3*np.pi*x) - 0.4*np.cos(4*np.pi*y) + 0.7