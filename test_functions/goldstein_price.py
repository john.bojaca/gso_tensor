import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función GoldsteinPrice"""
"""Mínimo Global => f(x) = 3, en x = (0,-1)"""
class GoldsteinPrice(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -2.0, 2.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return GoldsteinPrice.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        x = coordinates[:,0]
        y = coordinates[:,1]

        return (1 + torch.pow((x + y + 1), 2)*(19 - 14*x + 3*torch.pow(x, 2) - 14*y + 6*x*y + 3*torch.pow(y, 2))) * (30 + torch.pow((2*x - 3*y), 2)*(18 - 32*x + 12*torch.pow(x, 2) + 48*y -36*x*y + 27*torch.pow(y, 2)))
    
    def PlotCalculate(self, x, y):
        return (1 + np.power((x + y + 1), 2)*(19 - 14*x + 3*np.power(x, 2) - 14*y + 6*x*y + 3*np.power(y, 2))) * (30 + np.power((2*x - 3*y), 2)*(18 - 32*x + 12*np.power(x, 2) + 48*y -36*x*y + 27*np.power(y, 2)))