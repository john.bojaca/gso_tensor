import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Suma de Cuadrados"""
"""Mínimo Global => f(x, y) = 0, en x = (0,0)"""
class SumSquares(ObjectiveFunction):
    def __init__(self):        
        dimension = 4
        self.setSpace(Space(dimension, -6.0, 6.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        
        if dimension == 2:
            self.PlotSurface()

    def __call__(self, coordinates):
        return SumSquares.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")

        for i in range(0, dimension):
            first = first + (i+1)*torch.pow(coordinates[:,i], 2)

        return first
    
    def PlotCalculate(self, x, y):
        return np.power(x, 2) + 2*np.power(y, 2)