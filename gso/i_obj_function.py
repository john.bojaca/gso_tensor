from abc import abstractmethod
from abc import ABCMeta

"""Interface para la Función Objetivo"""
"""Es la función a optimizar usando en el Algoritmo GSO"""
class IObjectiveFunction(metaclass=ABCMeta):
    """Interfaz para la Funcion Objetivo"""
    @abstractmethod
    def __call__(self, coordinates):
        pass
    
    @abstractmethod
    def PlotCalculate(self, x, y):
        pass
