import torch
from gso import GSOAlgorithm
import test_functions
import nn

obj_function = nn.SimpleMPL()
#obj_function = test_functions.Ackley()

gso = GSOAlgorithm(obj_function)
gso.run(100)

if obj_function.getParameters().isFindMin():
    index = torch.argmin(gso.getSwarm().getScoring())
else:
    index = torch.argmax(gso.getSwarm().getScoring())

best_coordinates = gso.getSwarm().getCoordinates()[index]
print(f'Indice {index} - Coordenadas de la Mejor Luciernaga: {best_coordinates}')

if isinstance(obj_function, nn.NeuralNetwork):
    outputs = obj_function.TestNetwork(best_coordinates)
