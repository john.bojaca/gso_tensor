from gso.gso_parameters import GSOParameters
from gso.i_obj_function import IObjectiveFunction
from gso.obj_function import ObjectiveFunction
from gso.space import Space
from nn.dataset_deepslice import DSDeepSlice
from nn.neural_network import NeuralNetwork
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torch.nn as nn
import torch
import pandas as pd
from sklearn.model_selection import train_test_split

class SimpleMPL(ObjectiveFunction, IObjectiveFunction, NeuralNetwork):
    def __init__(self):
        self.showOutputsLayers = False
        self.LoadData()
        self.CreateModel()

        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        self.setSpace(Space(self.GetVariables(), -1.0, 1.0))
        print(self.model)
    
    def PlotCalculate(self, x, y):
        pass

    """Carga la información para entrenamiento y test para Red Neuronal"""
    def LoadData(self):
        #self.data = torch.tensor([[0., 0., 0.], [0., 1., 0.], [1., 0., 0.], [1., 1., 1.]], device="cuda") ## Compuerta AND
        #self.data = torch.tensor([[0., 0., 0.], [0., 1., 1.], [1., 0., 1.], [1., 1., 1.]], device="cuda") ## Compuerta OR
        self.data = torch.tensor([[0., 0., 0.], [0., 1., 1.], [1., 0., 1.], [1., 1., 0.]], device="cuda") ## Compuerta XOR

        #self.data = torch.tensor([[-1., -1., -1.], [-1., 1., -1.], [1., -1., -1.], [1., 1., 1.]], device="cuda") ## Compuerta AND
        #self.data = torch.tensor([[-1., -1., -1.], [-1., 1., 1.], [1., -1., 1.], [1., 1., 1.]], device="cuda") ## Compuerta OR
        #self.data = torch.tensor([[-1., -1., -1.], [-1., 1., 1.], [1., -1., 1.], [1., 1., -1.]], device="cuda") ## Compuerta XOR

        self.LoadDataTraining()
        self.LoadDataTest()

    """Carga la información para el entrenamiento de la Red neuronal"""
    def LoadDataTraining(self):
        self.dataTrain = self.data
        print(f"Datos de Entrenamiento cargados ... Número de registros {self.dataTrain.__len__()}")

    """Carga la información para las pruebas de la red neuronal"""
    def LoadDataTest(self):
        self.dataTest = self.data
        print(f"Datos de Prueba cargados ... Número de registros {self.dataTest.__len__()}")

    """Retorna los datos de entrada para el entrenamiento"""
    def GetInputTraining(self):
        return self.dataTrain[:,:2]

    """Retorna los datos de salida para el entrenamiento"""
    def GetOutputTraining(self):
        return self.dataTrain[:,2]

    """Retorna los datos de entrada para las pruebas"""
    def GetInputTest(self):
        return self.dataTrain[:,:2]

    """Retorna los datos de salida para las pruebas"""
    def GetOutputTest(self):
        return self.dataTrain[:,2]

    """Crea el modelo de la red neuronal"""
    def CreateModel(self):
        input_dim  = 2
        hidden_dim = 2
        output_dim = 1

        self.model = MLPnet(input_dim, hidden_dim, output_dim)
        self.model = self.model.cuda()

        self.dimensions = 0
        for param_tensor in self.model.state_dict():
            self.dimensions += self.model.state_dict()[param_tensor].nelement()
        
        self.train_loader = self.dataTrain
        self.test_loader = self.dataTest

        self.error = nn.CrossEntropyLoss()
    
    """Retornal el número de variables de la Red Neuronal"""
    def GetVariables(self):
        return self.dimensions

    """Llamado del Algoritmo GSO con las coordenadas de las Luciernagas"""
    def __call__(self, coordinates):
        return self.TrainNetwork(coordinates)

    """Entrena la Red Neuronal con el Algotirmo GSO"""
    def TrainNetwork(self, coordinates):
        num_glowworms = coordinates.size(dim=0)
        values = torch.zeros(num_glowworms, device="cuda")
        
        total = len(self.dataTrain)
        
        self.model.eval()
        with torch.no_grad():

            for glowworm in range(num_glowworms):
                self.___setCoordinatesInModel___(coordinates[glowworm])

                outputs = self.model(self.GetInputTraining())

                mse = torch.sum(torch.pow(outputs.view(1, total) - self.GetOutputTraining(), 2)) / total
                values[glowworm] = mse
            
            index = torch.argmin(values)
            best = coordinates[index]
            self.___setCoordinatesInModel___(best)

            outputs = self.model(self.GetInputTest())
            predicted = self.GetOutputTest()

            total = len(self.dataTest)
            correct = (predicted == outputs.view(1,total)).sum()

            print(f'Accuaracy: {(correct/total)*100}')

        return values
    
    def ___setCoordinatesInModel___(self, coordinate):
        sd = self.model.state_dict()
        init_index = 0
        #print(f"Coordenadas: {coordinate}")
        
        for param_tensor in sd:
            size_layer = sd[param_tensor].nelement()
            param = coordinate[init_index : init_index + size_layer]
            param = param.reshape(self.model.state_dict()[param_tensor].size())
            sd[param_tensor] = param
            init_index = init_index + size_layer
        
        self.model.load_state_dict(sd)

    """Prueba la Red Neuronal con los datos de Test"""
    def TestNetwork(self, coordinate):
        self.___setCoordinatesInModel___(coordinate=coordinate)
        self.model.IsShowOutputsLayers(True)
        return self.model(self.GetInputTest())


    """Guarda las coordenadas de la Luciernaga con la mejor puntuación"""
    def SaveNetwork(self, location):
        pass

    """Lee los valores guardados y los carga en el modelo"""
    def LoadNetwork(self, location):
        pass

    """Evalua el resultado de una entrada con la red neuronal cargada"""
    def Evaluate(self, data):
        pass

class MLPnet(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim):
        super(MLPnet, self).__init__()
        self.__showOutputsLayers = False
        self.__input_dim = input_dim

        self.__linear1 = nn.Linear(input_dim, hidden_dim) 
        self.__relu1 = nn.ReLU()
        self.__linear2 = nn.Linear(hidden_dim, hidden_dim) 
        self.__relu2 = nn.ReLU()
        self.__linear3 = nn.Linear(hidden_dim, output_dim)
        self.__tanh = nn.Tanh()
        #self.__softmax = nn.Softmax(dim=1)  

    def forward(self, x):
        x = x.view(-1, self.__input_dim)

        z1 = self.__linear1(x)
        if self.__showOutputsLayers:
            print(f"Z1: {z1}")
        
        y1 = self.__relu1(z1)
        if self.__showOutputsLayers:
            print(f"Y1: {y1}")
        
        z2 = self.__linear2(y1)
        if self.__showOutputsLayers:
            print(f"Z2: {z2}")
        
        y2 = self.__relu2(z2)
        if self.__showOutputsLayers:
            print(f"Y2: {y2}")
        
        z3 = self.__linear3(y2)
        if self.__showOutputsLayers:
            print(f"Z3: {z3}")
        
        y3 = self.__tanh(z3)

        y3[y3>=0.5] = 1.
        y3[y3<0.5] = 0.

        if self.__showOutputsLayers:
            print(f"Y3: {y3}")

        return y3
    
    def name(self):
        return "MLP"
    
    def IsShowOutputsLayers(self, show):
        self.__showOutputsLayers = show