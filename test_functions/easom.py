import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Easom"""
"""Mínimo Global => f(x, y) = -1 en (pi, pi)"""
class Easom(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -5.0, 5.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return Easom.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        x = coordinates[:,0]
        y = coordinates[:,1]

        return -torch.cos(x)*torch.cos(y)*torch.exp(-torch.pow(x - torch.pi, 2) - torch.pow(y - torch.pi, 2))
    
    def PlotCalculate(self, x, y):
        return -np.cos(x)*np.cos(y)*np.exp(-np.power(x - np.pi, 2) - np.power(y - np.pi, 2))