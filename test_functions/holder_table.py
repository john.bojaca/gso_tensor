import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función HolderTable"""
"""Mínimo Global => f(x, y) = -19.2085, en (8.05502, 9.66459), (8.05502, -9.66459), (-8.05502, 9.66459), (-8.05502, -9.66459)"""
class HolderTable(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -10.0, 10.0))
        self.setNumGlowworms(300)
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return HolderTable.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        x = coordinates[:,0]
        y = coordinates[:,1]

        return -torch.abs(torch.sin(x)*torch.cos(y)*torch.exp(torch.abs(1 - (torch.sqrt(torch.pow(x, 2) + torch.pow(y ,2)) / torch.pi))))
    
    def PlotCalculate(self, x, y):
        return -np.abs(np.sin(x)*np.cos(y)*np.exp(np.abs(1 - (np.sqrt(np.power(x, 2) + np.power(y ,2)) / np.pi))))