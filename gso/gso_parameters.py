"""Definición de Parametros del Algoritmo"""

class GSOParameters(object):
    def __init__(self, rho=0.4, gamma=0.6, beta=0.08, ini_luciferin=0.3, ini_vision_range=0.4, max_vision_range=3.0, max_neighbors=5, step_size=0.06,  verbose=False, find_min=False) :
        self.__rho = rho
        self.__gamma = gamma
        self.__beta = beta
        self.__ini_luciferin = ini_luciferin
        self.__ini_vision_range = ini_vision_range
        self.__max_vision_range = max_vision_range
        self.__max_neighbors = max_neighbors
        self.__step_size = step_size
        self.__verbose = verbose
        self.__find_min = find_min
    
    def getBeta(self):
        return self.__beta
    
    def getGamma(self):
        return self.__gamma
    
    def getRho(self):
        return self.__rho
    
    def getIniLuciferin(self):
        return self.__ini_luciferin
    
    def getIniVisionRange(self):
        return self.__ini_vision_range
    
    def getMaxNeighbors(self):
        return self.__max_neighbors
    
    def getMaxVisionRange(self):
        return self.__max_vision_range
    
    def getStepSize(self):
        return self.__step_size
    
    def isFindMin(self):
        return self.__find_min
    
    def isVerbose(self):
        return self.__verbose