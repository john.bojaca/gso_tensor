import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Rastrigin"""
"""Mínimo Global => f(x*) = 0, en (0,...,0)"""
class Rastrigin(ObjectiveFunction):
    def __init__(self):
        dimension = 2
        self.setSpace(Space(dimension, -5.0, 5.0))
        self.setNumGlowworms(1000)
        self.setParameters(GSOParameters(find_min=True))
        
        if dimension == 2:
            self.PlotSurface()

    def __call__(self, coordinates):
        return Rastrigin.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):           
        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")

        for i in range(0, dimension):
            first = first + (torch.pow(coordinates[:,i] , 2) - 10*torch.cos(2*torch.pi*coordinates[:,i]))

        return 10*dimension + first
    
    def PlotCalculate(self, x, y):
        return 20.0 + (np.power(x, 2) - 10.0 * np.cos(2 * np.pi * x) + y * y - 10.0 * np.cos(2 * np.pi * y))