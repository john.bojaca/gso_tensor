import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Matyas"""
"""Mínimo Global => f(x, y) = 0, en (0, 0)"""
class Matyas(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -10.0, 10.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return Matyas.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        x = coordinates[:,0]
        y = coordinates[:,1]

        return 0.26*(torch.pow(x, 2) + torch.pow(y,2)) - 0.48*x*y
    
    def PlotCalculate(self, x, y):
        return 0.26*(np.power(x, 2) + np.power(y,2)) - 0.48*x*y