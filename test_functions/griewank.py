import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Griewank"""
"""Mínimo Global => f(x, y) = 0, en x = (0,0)"""
class Griewank(ObjectiveFunction):
    def __init__(self):
        dimension = 2
        self.setSpace(Space(dimension, -5.0, 5.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))

        if dimension == 2:
            self.PlotSurface()

    def __call__(self, coordinates):
        return Griewank.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")
        second = torch.ones(glowworms, dtype=torch.float, device="cuda")

        for i in range(0, dimension):
            first = first + torch.pow(coordinates[:,i], 2) / 4000
            second = second * torch.cos(coordinates[:,i]/np.sqrt(i+1))

        return first - second + 1
    
    def PlotCalculate(self, x, y):
        return ((np.power(x, 2) + np.power(y, 2)) / 4000) - (np.cos(x/np.sqrt(1))*np.cos(y/np.sqrt(2))) + 1