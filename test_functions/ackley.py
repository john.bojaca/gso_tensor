from gso.i_obj_function import IObjectiveFunction
import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Ackley n-dimensiones"""
"""Mínimo Global => f(x*) = 0, en x* = (0,...,0)"""
class Ackley(ObjectiveFunction, IObjectiveFunction):
    def __init__(self):
        dimension = 2
        self.setSpace(Space(dimension, -32.0, 32.0))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(step_size=0.5, max_vision_range=60, ini_vision_range=6, find_min=True))

        print(f"Salidas: {torch.zeros(dimension, dtype=torch.float, device='cuda')}")
        
        if dimension == 2:
            self.PlotSurface()

    def __call__(self, coordinates):
        return Ackley.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        a = 20
        b = 0.2
        c = 2*torch.pi

        glowworms = coordinates.size()[0]
        dimension = coordinates.size()[1]

        first = torch.zeros(glowworms, dtype=torch.float, device="cuda")
        second = torch.zeros(glowworms, dtype=torch.float, device="cuda")

        for i in range(0, dimension):
            first = first + torch.pow(coordinates[:, i], 2)
            second = second + torch.cos(c*coordinates[:, i])

        return -a*torch.exp(-b*torch.sqrt((1/dimension)*first)) - torch.exp((1/dimension)*second) + a + torch.e
    
    def PlotCalculate(self, x, y):
        return -20.0*np.exp(-0.2*np.sqrt(0.5*(x**2 + y**2))) - np.exp(0.5*(np.cos(2*np.pi*x) + np.cos(2*np.pi*y))) + np.e + 20