import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Drop Wave"""
"""Mínimo Global => f(x, y) = -1, en x = (0,0)"""
class DropWave(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -3, 3))
        self.setNumGlowworms(300)
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return DropWave.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):
        x = coordinates[:,0]
        y = coordinates[:,1]

        return -(1 + torch.cos(12*torch.sqrt(torch.pow(x, 2) + torch.pow(y, 2)))) / (0.5*(torch.pow(x, 2) + torch.pow(y, 2)) + 2)
    
    def PlotCalculate(self, x, y):
        return -(1 + np.cos(12*np.sqrt(np.power(x, 2) + np.power(y, 2)))) / (0.5*(np.power(x, 2) + np.power(y, 2)) + 2) 