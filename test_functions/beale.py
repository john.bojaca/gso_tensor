from gso.i_obj_function import IObjectiveFunction
import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Beale"""
"""Mínimo Global => f(x,y) = 0 en (3, 0.5)"""
class Beale(ObjectiveFunction, IObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -4.5, 4.5))
        self.setNumGlowworms(200)
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

        print("Salidas: 3.0, 0.0")

    def __call__(self, coordinates):
        return Beale.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        x = coordinates[:,0]
        y = coordinates[:,1]

        return torch.pow((1.5 - x + x*y), 2) + torch.pow((2.25 - x + x*torch.pow(y,2)), 2) + torch.pow((2.625 - x + x*torch.pow(y,3)), 2)
    
    def PlotCalculate(self, x, y):
        return np.power((1.5 - x + x*y), 2) + np.power((2.25 - x + x*np.power(y,2)), 2) + np.power((2.625 - x + x*np.power(y,2)), 2)