import  torch
import numpy as np
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función Three Hump Camel"""
"""Mínimo Global => f(x) = 0, en x = (0,0)"""
class ThreeHumpCamel(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(2, -5.0, 5.0))
        self.setParameters(GSOParameters())
        self.setNumGlowworms(200)
        self.PlotSurface()

    def __call__(self, coordinates):
        return ThreeHumpCamel.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        x = coordinates[:,0]
        y = coordinates[:,1]

        return 1 / (2*torch.pow(x, 2) - 1.05*torch.pow(x, 4) + (torch.pow(x, 6) / 6) + x*y + torch.pow(y, 2))
    
    def PlotCalculate(self, x, y):
        return 2*np.power(x, 2) - 1.05*np.power(x, 4) + (np.power(x, 6) / 6) + x*y + np.power(y, 2)