import  torch
import numpy as np
from gso.dimension import Dimension
from gso.gso_parameters import GSOParameters
from gso.obj_function import ObjectiveFunction
from gso.space import Space

"""Función McCormick"""
"""Mínimo Global => f(x, y) = -1.9133, en x = (-0.54719, -1.54719)"""
class McCormick(ObjectiveFunction):
    def __init__(self):
        self.setSpace(Space(0, 0, 0))

        self.getSpace().addDimension(Dimension(0, -1.5, 4))
        print(f'Creando... {self.getSpace().getDimension(0).__repr__()}')
        self.getSpace().addDimension(Dimension(1, -3, 4))
        print(f'Creando... {self.getSpace().getDimension(1).__repr__()}')

        self.setNumGlowworms(200)
        
        self.setParameters(GSOParameters(find_min=True))
        self.PlotSurface()

    def __call__(self, coordinates):
        return McCormick.calculate(coordinates)
    
    @staticmethod
    def calculate(coordinates):        
        x = coordinates[:,0]
        y = coordinates[:,1]

        return torch.sin(x + y) + torch.pow(x - y, 2) - 1.5*x + 2.5*y + 1
    
    def PlotCalculate(self, x, y):
        return np.sin(x + y) + np.power(x - y, 2) - 1.5*x + 2.5*y + 1