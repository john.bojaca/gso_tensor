from cmath import inf
from gso.swarm import Swarm
import numpy as np
import torch
import matplotlib.pyplot as plt
from matplotlib import cm

class GSOAlgorithm(object):
    """Constructor de la Simulación de algoritmo GSO"""
    def __init__(self, obj_function):
        self.__obj_function = obj_function
        self.__swarm = Swarm(obj_function)
    
    """Devuelve el objeto Swarm"""
    def getSwarm(self):
        return self.__swarm

    """Corre la simulación para cada uno de los pasos"""
    def run(self, simulation_steps):
        plt.ion()

        x1, y1 = [],[]
        x2, y2 = [],[]

        if self.__obj_function.getSpace().getNumDimension() == 2 :
            fig, ax = plt.subplots(2, 2)
            fig.tight_layout(pad=1.5)

            sc1 = ax[0, 0].scatter(x1, y1, s=1)
            ax[0, 0].set_xlabel('Iteraciones', fontsize=8)
            ax[0, 0].set_ylabel('Distancia', fontsize=8)
            ax[0, 0].set_title('Distancia relativa a la primera Luciernaga', fontsize=12)
            ax[0, 0].set_xlim(0, simulation_steps)

            sc2 = ax[0, 1].scatter(x2, y2, s=1)
            ax[0, 1].set_xlabel('Iteraciones', fontsize=8)
            ax[0, 1].set_ylabel('Puntuación', fontsize=8)
            ax[0, 1].set_title('Puntuaciones de las Luciernagas por Iteración', fontsize=12)
            ax[0, 1].set_xlim(0, simulation_steps)

            xlist = np.linspace(self.__obj_function.getSpace().getDimension(0).getLowerLimit(), self.__obj_function.getSpace().getDimension(0).getUpperLimit(), 100)
            ylist = np.linspace(self.__obj_function.getSpace().getDimension(1).getLowerLimit(), self.__obj_function.getSpace().getDimension(1).getUpperLimit(), 100)
            X, Y = np.meshgrid(xlist, ylist)
            Z = self.__obj_function.PlotCalculate(X, Y)
            ax[1, 0].contourf(X, Y, Z, cmap=cm.coolwarm)

            x, y = [],[]
            sc = ax[1, 0].scatter(x, y, s=1, c='lime', marker='.')
            ax[1, 0].set_xlabel('x', fontsize=8)
            ax[1, 0].set_ylabel('y', fontsize=8)
            ax[1, 0].set_title('Coordenadas de las Luciernagas', fontsize=12)
            ax[1, 0].set_xlim(self.__obj_function.getSpace().getDimension(0).getLowerLimit(), self.__obj_function.getSpace().getDimension(0).getUpperLimit())
            ax[1, 0].set_ylim(self.__obj_function.getSpace().getDimension(1).getLowerLimit(), self.__obj_function.getSpace().getDimension(1).getUpperLimit())

            ax[1, 1].contourf(X, Y, Z, cmap=cm.coolwarm)
            ax[1, 1].set_xlabel('x', fontsize=8)
            ax[1, 1].set_ylabel('y', fontsize=8)
            ax[1, 1].set_title('Coordenadas Finales de las Luciernagas', fontsize=12)
            ax[1, 1].set_xlim(self.__obj_function.getSpace().getDimension(0).getLowerLimit(), self.__obj_function.getSpace().getDimension(0).getUpperLimit())
            ax[1, 1].set_ylim(self.__obj_function.getSpace().getDimension(1).getLowerLimit(), self.__obj_function.getSpace().getDimension(1).getUpperLimit())
        
        else:
            fig, ax = plt.subplots(2, 1)
            fig.tight_layout(pad=1.5)

            sc1 = ax[0].scatter(x1, y1, s=1)
            ax[0].set_xlabel('Iteraciones', fontsize=8)
            ax[0].set_ylabel('Distancia', fontsize=8)
            ax[0].set_title('Distancia relativa a la primera Luciernaga', fontsize=12)
            ax[0].set_xlim(0, simulation_steps)

            sc2 = ax[1].scatter(x2, y2, s=1)
            ax[1].set_xlabel('Iteraciones', fontsize=8)
            ax[1].set_ylabel('Puntuación', fontsize=8)
            ax[1].set_title('Puntuaciones de las Luciernagas por Iteración', fontsize=12)
            ax[1].set_xlim(0, simulation_steps)

        plt.draw()

        for step in range(1, simulation_steps + 1):
            print(f'Iteación número {step}')
            self.__InfoLog("")

            if self.__obj_function.getSpace().getNumDimension() == 2 :
                """x, y = [],[]"""
                x_step = self.getSwarm().getCoordinates()[:,0].cpu().detach().numpy()
                y_step = self.getSwarm().getCoordinates()[:,1].cpu().detach().numpy()
                x.extend(x_step)
                y.extend(y_step)
                sc.set_offsets(np.c_[x,y])

            self.__InfoLog("--- Fase de Actualización de Lucerferina ---")
            self.getSwarm().update_luciferin()
            self.__InfoLog("")

            self.__InfoLog("--- Fase de Movimiento ---")
            self.getSwarm().movement_phase()
            self.__InfoLog("")

            self.__InfoLog("--- Fase de Actualización de Rangos de Visión ---")
            self.getSwarm().update_vision_range()
            self.__InfoLog("")

            distances = self.getSwarm().getDistances()[0].cpu().detach().numpy()
            scores = self.getSwarm().getScoring().cpu().detach().numpy()
            xPlot = np.ones(scores.size)*step

            scores[scores == np.inf] = 1000000000000
            scores[scores == -np.inf] = -1000000000000

            x1.extend(xPlot)
            y1.extend(distances)

            sc1.set_offsets(np.c_[x1, y1])
            max = np.max(y1)
            min = np.min(y1)
            diff = max - min

            if self.__obj_function.getSpace().getNumDimension() == 2 :
                ax[0, 0].set_ylim(min - diff*0.1, max + diff*0.1)
            else:
                ax[0].set_ylim(min - diff*0.1, max + diff*0.1)

            x2.extend(xPlot)
            y2.extend(scores)

            sc2.set_offsets(np.c_[x2, y2])
            max = np.max(y2)
            min = np.min(y2)

            if max == np.Inf :
                max = 1000000000000
            if max == -np.Inf :
                max = -1000000000000
            if min == np.Inf :
                min = 1000000000000
            if min == -np.Inf :
                min = -1000000000000

            diff = max - min

            if self.__obj_function.getSpace().getNumDimension() == 2 :
                ax[0, 1].set_ylim(min - diff*0.1, max + diff*0.1)
            else:
                ax[1].set_ylim(min - diff*0.1, max + diff*0.1)

            fig.canvas.draw_idle()
            plt.pause(0.0001)

        if self.__obj_function.getSpace().getNumDimension() == 2 :
            ax[1, 1].scatter(x_step, y_step, s=5, c='lime')
            
        plt.waitforbuttonpress()
    
    """Muestra el Log para esta clase"""
    def __InfoLog(self, message):
        if self.__obj_function.getParameters().isVerbose():
            print(message) 